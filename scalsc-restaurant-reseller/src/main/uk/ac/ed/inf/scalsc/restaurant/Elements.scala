package uk.ac.ed.inf.scalsc.restaurant

import com.moseph.scalsc._
import com.moseph.scalsc.prolog._
import com.moseph.scalsc.utility.LogHelpers

trait Food {
  def name:String
  def cost:Double
}

case object NullFood extends Food {
  override val name = "(null)"
  override val cost = Double.NaN
}

case object Want extends BinOp { override def value = "want" }
case object IsFoodCoordinator extends Op { override def value = "isFoodCoordinator" }
case object CheckOffer extends Op { override def value = "checkOffer" }
case object FailOffer extends Op { override def value = "failOffer" }
case object ElicitOffer extends Op { override def value = "elicitOffer" }

case object Accepted extends Op { override def value = "accepted" }
case object Rejected extends Op { override def value = "rejected" }
case object Profit extends BinOp { override def value = "profit" }
case object Sold extends BinOp { override def value = "sold" }


class Calendar(val days : Int, total:Int) extends LogHelpers {
  var day:Int = 1
  var count:Int = 0
  
  def notifyDone() = {
    count += 1
    if (count == total && day < days) {
      day += 1
      //info(" ! [NEWDAY] : " + day)
      count = 0
    }
  }
  
  override def toString = "Calendar(" + day + "/" + days + ", " + total + ")"
}


case class Customer(name:String, foodCoordinator:String, prefs:List[(String,String,Double)], prefsI:Int, calendar: Calendar, day:Int) extends ComputationInterface with LogHelpers {
  def getWant(foodVar:String,restaurantVar:String) = {
    prefs.drop(prefsI).headOption match {
      case None => {
        None //Some(Subst(),this)
      }
      case Some((food,restaurant,_)) => {
        val s = Subst((foodVar,Atom(food)),(restaurantVar,Atom(restaurant))) 
        Some(s,this.copy(prefsI = prefsI+1))
      }   
    }
  }
  
  def checkOffer(food:String, restaurant:String, price:Double) = {
    prefs.drop(prefsI-1).headOption match {
      case Some((food,restaurant,p)) => price <= p 
      case _ => false
    }
  }
  
  def statusString(prefs:List[(String,String,Double)]) = {
    ("" /: (prefs map {case (f,r,p)=>"[" + f + "," + r + ":" + p + "] "})) (_+_)
  }

  
  def assert ( a: Term ) :(Boolean,ComputationInterface) = {
		if (Accepted check a) {
      Accepted dest a match {
        case Seq(food,cost,restaurant) => {
          //info(" ! [ORDERED] [" + name + "] Day " + day + ": [" + food.asString + "] from [" + restaurant.asString + "] at [" + cost.asString + "].")
          calendar.notifyDone()
          (true,copy(prefsI=prefs.size)) // halt condition, otherwise we keep buying
        }
        case _ => (false,this)
      }      
    } else if (Rejected check a) {
      Rejected dest a match {
        case Seq(food,cost,restaurant) => {
          if (prefsI == prefs.size) {
            info(" ! [REJECTED] [" + name + "] Day " + day + ": Did not buy any food.")
            calendar.notifyDone()
          }
          (true,this)
        }
        case _ => (false,this)
      }      
    }
    else (false,this)
  }
  
  def query ( q: Term ) :Option[(Subst,ComputationInterface)] = {
    if (Want check q) {
      Want destBin q match {
        case (Var(foodVar),Var(restaurantVar)) => getWant(foodVar,restaurantVar)
        case _ => None
      }      
    } 
    else if (IsFoodCoordinator check q) {
      IsFoodCoordinator.dest(q).headOption match {
        case Some(Var(v)) => Some(Subst((v,Atom(foodCoordinator))),this)
        case _ => None
      }
    }
    else if (CheckOffer check q) {
      CheckOffer dest q match {
        case Seq(foodTerm:Literal[_],restaurantTerm:Literal[_],Number(offer)) if checkOffer(foodTerm.asString,restaurantTerm.asString,offer) => Some(Subst(),this)
        case _ => None
      }
    } 
    else if (FailOffer check q) {
      FailOffer dest q match {
        case Seq(foodTerm:Literal[_],restaurantTerm:Literal[_],Number(offer)) if !checkOffer(foodTerm.asString,restaurantTerm.asString,offer) => Some(Subst(),this)
        case _ => None
      }
    } 
    else None
  }
  
  def update() :Option[ComputationInterface] = {
    //info("[UPDATE!] " + calendar.day + " > " + day + " ???")
    if (calendar.day > day) Some(copy(prefsI=0,day=calendar.day))
    else None
  }
}

case object CustomerInitState {
  def apply(customer:Customer, logLevel:AgentStateLogger.LogLevel) = {
    val id = customer.name + "Agent"
    val k = PrologParser.parsing("start("+customer.name+"I,customer,[],\"restaurant-reseller.inst\")")(PrologParser.term)
    AgentState(ID(id),DefaultAgentStateLogger(logLevel),MultiComputationInterface(Seq(customer,PrologSatisfier(Seq(k)))))
  }    
}




case class Restaurant(name:String, sellerID:ID, foods:Map[String,(Food,Double)], demand:Map[String,Int], behaviour:RestaurantBehaviour, calendar: Calendar, day:Int) extends ComputationInterface with LogHelpers {
  
  def newDay(newDay:Int) = {
    val newfoods = foods map {case (k,(f,_))=>(k, (f,behaviour.determineNewPrice(k,this)))}
    val newdemand = Map[String,Int]()
    info(" ! [PRICES] [" + name + "] Day " + newDay + ": " + statusString(newfoods,demand))
    copy(foods=newfoods,demand=newdemand,behaviour=behaviour.update(this),day=newDay)
  }
  
  def statusString(foods:Map[String,(Food,Double)],demand:Map[String,Int]) = {
    ("" /: (foods map {case (k,(_,p))=>"[" + k + ":" + demand.getOrElse(k,0) + ":" + p + "] "})) (_+_)
  }
  
  def incDemand(food:String) :Map[String,Int] = demand + (food -> (demand.getOrElse(food, 0) + 1))
  
  def assert ( a: Term ) :(Boolean,ComputationInterface) = {
		if (Sold check a) {
      val (food,cost) = Sold destBin a
    	//info(" ! [SOLD] [" + name + "] Day " + day + ": [" + food.asString + "] at [" + cost.asString + "].")
      val newR = if (calendar.day > day) copy(demand=incDemand(food.asString)).newDay(calendar.day) else copy(demand=incDemand(food.asString))
      (true,newR)
    }
    else (false,this)
  }
  
  def query ( q: Term ) :Option[(Subst,ComputationInterface)] = {
    if (ElicitOffer check q) {
      ElicitOffer dest q take 3 match {
        case Seq(foodTerm:Literal[_],Var(cost),Var(restaurant)) => {
          val s = Subst((cost,Number(foods.getOrElse(foodTerm.asString, (NullFood,Double.MaxValue))._2)),(restaurant,sellerID))
          Some(s,this)
        }
        case _ => None
      }
    } 
    else None
  }
  
  def update() :Option[ComputationInterface] = None
}

case object RestaurantInitState {
  def apply(restaurant:Restaurant, logLevel:AgentStateLogger.LogLevel) = {
    val id = restaurant.name + "Agent"
    val k1 = PrologParser.parsing("start("+restaurant.name+",restaurantSeller,[],\"restaurant-reseller.inst\")")(PrologParser.term)
    val k2 = PrologParser.parsing("start("+restaurant.sellerID+",restaurant,[],\"restaurant-reseller.inst\")")(PrologParser.term)
    AgentState(ID(id),DefaultAgentStateLogger(logLevel),MultiComputationInterface(Seq(restaurant,PrologSatisfier(Seq(k1,k2)))))
  }    
}




case object CoordinatorInitState {
  def apply(id:String, iids:Seq[String], commission:Double, logLevel:AgentStateLogger.LogLevel) = {
    val k = iids map { i => PrologParser.parsing("start("+i+",foodCoordinator,["+commission+"],\"restaurant-reseller.inst\")")(PrologParser.term)}
    AgentState(ID(id),DefaultAgentStateLogger(logLevel),PrologSatisfier(k))
  }
}