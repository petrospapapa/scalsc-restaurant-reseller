package uk.ac.ed.inf.scalsc.restaurant

import com.moseph.scalsc._
import com.moseph.scalsc.prolog._

case object Pie extends Food {
  override val name = "pie"
  override val cost = 5d
}

case object Burger extends Food {
  override val name = "burger"
  override val cost = 10d
}

case object Steak extends Food {
  override val name = "steak"
  override val cost = 20d
}


case object PieLover {
  def apply(name:String, coordinator:String, allowance:Double, restaurants:List[String], calendar:Calendar) = {
    def pref(r:String) = Seq((Pie.name,r,allowance*1d),(Steak.name,r,allowance*1d),(Burger.name,r,allowance*1d))
    Customer(name,coordinator,restaurants flatMap pref,0,calendar,1)
  }
}
  
case object BurgerLover {
  def apply(name:String, coordinator:String, allowance:Double, restaurants:List[String], calendar:Calendar) = {
    def pref(r:String) = Seq((Burger.name,r,allowance*1d),(Steak.name,r,allowance*1d),(Pie.name,r,allowance*1d))
    Customer(name,coordinator,restaurants flatMap pref,0,calendar,1)
  }
}

case object SteakLover {
  def apply(name:String, coordinator:String, allowance:Double, restaurants:List[String], calendar:Calendar) = {
    def pref(r:String) = Seq((Steak.name,r,allowance*1d),(Burger.name,r,allowance*1d),(Pie.name,r,allowance*1d))
    Customer(name,coordinator,restaurants flatMap pref,0,calendar,1)

  }
}
