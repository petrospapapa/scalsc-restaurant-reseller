package uk.ac.ed.inf.scalsc.restaurant

trait RestaurantBehaviour {
  def determineNewPrice(food:String, restaurant:Restaurant) :Double
  def update(restaurant:Restaurant) :RestaurantBehaviour
}

trait CustomerBehaviour {
  def determineNewPrice(food:String, restaurant:Restaurant) :Double
  def update(restaurant:Restaurant) :RestaurantBehaviour
}

case class SimpleRestaurantBehaviour(previousDemand:Map[String,Int]) extends RestaurantBehaviour {
  def determineNewPrice(food:String, restaurant:Restaurant) = {
    restaurant.foods.get(food) match {
      case Some((f,price)) => { 
        val margin = (price - f.cost) / f.cost // calculate margin
        val prevDemand = previousDemand.getOrElse(food, 0)
        val currDemand = restaurant.demand.getOrElse(food, 0)
        val diff = 
          if (prevDemand == 0 ) {
            if (currDemand == 0) -0.01
            else 0.01
          }
          else if (prevDemand == currDemand) 0.01
          else (currDemand - prevDemand) / prevDemand.toDouble * 0.8 * margin // discount

        val rdiff = if (diff > 0d && diff < 0.01d) 0.01d else if (diff < 0d && diff > -0.01d) -0.01d else diff
        val newmargin = margin + rdiff  // update margin
        val newprice = (math max(f.cost * newmargin,0.01d)) + f.cost // recalculate price
        (math rint newprice * 100d) / 100d
      }
      case _ => Double.MaxValue
    }
  }
  
  def update(restaurant:Restaurant) = copy(previousDemand=restaurant.demand)
}