package uk.ac.ed.inf.scalsc.restaurant

import scala.collection.immutable.Seq
import com.moseph.scalsc._
import com.moseph.scalsc.prolog._
import com.moseph.scalsc.environment.SimpleProtocolStore
import com.moseph.scalsc.utility._
import com.moseph.scalsc.simulation._

case class RestaurantResellerSimulator(protocol:Protocol,customers:Int,days:Int,foods:List[Food] = List(Pie,Burger,Steak),initialMargin:Double=0.1,resellerCommission:Double=0.1,logLevel:AgentStateLogger.LogLevel=AgentStateLogger.NONE) extends LogHelpers {
  val simulator = new Simulator(new SimpleProtocolStore(){},logLevel)
  simulator.addProtocol(protocol)
 
  // TODO getting stack overflows due to huge substitutions

  val minCost = foods map (_.cost) min
  val maxCost = foods map (_.cost) max
  val minAllowance = minCost + minCost * 0.11 // minimum margin is 11% (includes 10% of reseller)
  val maxAllowance = maxCost + maxCost * 0.4 // maximum margin is 40% (includes 10% of reseller)
  info (" - Allowance range: [" + minAllowance + "," + maxAllowance + "]")
  
  val r = scala.util.Random
  
  val calendar = new Calendar(days,customers)
  val foodMap = Map[String,(Food,Double)]() ++ (foods map {x => x.name->(x,x.cost + initialMargin * x.cost)})
  val restaurant = Restaurant("restaurant", ID("restaurantSeller"), foodMap, Map[String,Int](), SimpleRestaurantBehaviour(Map[String,Int]()), calendar, 1)
  info(" - [PRICES] [restaurant] Day 1: " + restaurant.statusString(foodMap,Map[String,Int]()))
 
  def getCoordinatorName(i:Int) = "fC" + i
    
  def getCustomerState(i:Int) = {
    val allowance = r.nextDouble() * (maxAllowance - minAllowance) + minAllowance
    val rallowance = (math rint allowance * 100d) / 100d
    val customer = SteakLover("customer" + i, getCoordinatorName(i), rallowance, List("restaurant"), calendar) 
    info (" - [NEW CUSTOMER " + i + "] Allowance:" + rallowance + " Prefs: " + customer.statusString(customer.prefs))
    CustomerInitState(customer,logLevel)
  }
  
  val customerStates = for (i <- 0 to customers-1) yield getCustomerState(i)
  val coordinatorIDs = for (i <- 0 to customers-1) yield getCoordinatorName(i)
  val sim = simulator.Simulation(Seq(CoordinatorInitState("Reseller",coordinatorIDs,resellerCommission,logLevel),RestaurantInitState(restaurant,logLevel)) ++ customerStates)
  def run() = {
    print_banner(" - [INIT STATE] "," " + sim)
    val fState = simulator.run(sim,false)
    print_banner(" - [FINAL STATE] "," " + fState)
  }
}

object RestaurantResellerSimulation extends App with LSCProtocolLoader {

   implicit val location = ResourceLocation("/protocols")
  
  load_protocol("restaurant-reseller.inst") match {
      case None => None
      case Some(p) => { 
        val t0 = System.nanoTime()
        RestaurantResellerSimulator(p,100,200).run()
        println("time: "+(System.nanoTime()-t0)/1e6+"ms")
      }
    }
}