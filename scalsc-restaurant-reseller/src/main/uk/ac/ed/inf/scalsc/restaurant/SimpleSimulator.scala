package uk.ac.ed.inf.scalsc.restaurant

import scala.collection.immutable.Seq
import com.moseph.scalsc._
import com.moseph.scalsc.prolog._
import com.moseph.scalsc.environment.SimpleProtocolStore
import com.moseph.scalsc.utility._
import com.moseph.scalsc.simulation._
import com.moseph.scalsc.lsc.Know

object CustomerKnowledge {
  def apply(id:String):Seq[Knowledge] = {
    implicit val parser = PrologParser.input_data
     List(
       PrologParser.parsing("known("+id+", isFoodCoordinator(restaurantResellerI) )"),
       PrologParser.parsing("known("+id+", want(pie,restaurantI) )"),
       PrologParser.parsing("known("+id+", checkOffer(pie,55,default) )"),
       PrologParser.parsing("known("+id+", start("+id+"I,customer,[],\"restaurant-reseller.inst\"))")
        )
  }
}

object CoordinatorKnowledge {
  def apply():Seq[Knowledge] = {
    implicit val parser = PrologParser.input_data
     List(
       PrologParser.parsing("known(restaurantReseller, start(restaurantResellerI,foodCoordinator,[0.1],\"restaurant-reseller.inst\"))")
        )
  }
}

object RestaurantKnowledge {
  def apply(id:String):Seq[Knowledge] = {
    implicit val parser = PrologParser.input_data
     List(
       PrologParser.parsing("known("+id+", elicitOffer(pie,50,default,restaurantR" + id + ") )"),
       PrologParser.parsing("known("+id+", start("+id+"I,restaurantSeller,[],\"restaurant-reseller.inst\"))"),
       PrologParser.parsing("known("+id+", start(restaurantR" + id + ",restaurant,[],\"restaurant-reseller.inst\"))")
        )
  }
}

case class SimpleRestaurantResellerSimulator(protocol:Protocol) extends LogHelpers {
  val simulator = new Simulator(new SimpleProtocolStore(){},AgentStateLogger.MIN)
  simulator.addProtocol(protocol)
  
  val knowledge = CustomerKnowledge("customer") ++ CoordinatorKnowledge() ++ RestaurantKnowledge("restaurant")
  def initState = simulator.init(knowledge)
  def run() = {
    print_banner(" - [INIT STATE] "," " + initState)
    val fState = simulator.run(initState)
    print_banner(" - [FINAL STATE] "," " + fState)
  }
//  
//  def run() = { 
//    val iState = initState
//    print_banner(" - [INIT STATE] "," " + iState)
//    val fState = simulator.run(initState)
//    print_banner(" - [FINAL STATE] "," " + fState)
//  }
//  
//  def run(n:Int) :Unit = {
//    val iState = initState
//    print_banner(" - [INIT STATE] "," " + iState)
//    run(n,n,iState)
//  }
//  
//  protected def run(n:Int,max:Int,state:simulator.Simulation) :Unit = {
//    val (res,newstate) = simulator.runOnce(state)
//    print_banner(" - [STATE] ",s"(${max-n+1}) state: $newstate")
//    if (res && n > 0) run(n-1,max,newstate)
//  }
//  
//  def withKnowledge(k:Seq[Knowledge]) = copy(knowledge=k)
//  def withData(data:Seq[String]) = copy(knowledge=data map {d=>PrologParser.parse(d)(PrologParser.input_data)})
//  def withData(data:String) = copy(knowledge=PrologParser.parse(data)(PrologParser.knowledgep))
}

object SimpleRestaurantResellerSimulation extends App with LSCProtocolLoader {

   implicit val location = ResourceLocation("/protocols")
  
//   SingleProtocolSimulator(protocolName,logLevel) match {
//    case None => println("Failed to load protocol:" + protocolName)
//    case Some(simulator) => 
//      simulator.run()
//  }  
  
  load_protocol("restaurant-reseller.inst") match {
      case None => None
      case Some(p) => {  
        SimpleRestaurantResellerSimulator(p).run()
      }
    }
}